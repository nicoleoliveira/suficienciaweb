# README #

Este repositório contém o projeto de exame de suficiência das disciplinas de desenvolvimento web I e II.

## Requisitos ##
* Django 1.8
* Python 2.7

## Do que se trata o projeto? ##

Este projeto trata da gestão de ocorrências de alunos. Onde o professor, pode cadastrar uma ocorrência que aconteceu com um aluno em determinada turma.

## Acesso servidor##
nicoleoliveira.pythonanywhere.com