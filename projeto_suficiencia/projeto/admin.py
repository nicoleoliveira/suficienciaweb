from django.contrib import admin
from projeto.models.professor import Professor
from projeto.models.aluno import Aluno
from projeto.models.ocorrencia import Ocorrencia

admin.site.register(Professor)
admin.site.register(Aluno)
admin.site.register(Ocorrencia)
