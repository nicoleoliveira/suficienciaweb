from django.db import models
from django.contrib.auth.models import User


class Aluno(User):
    """docstring for Aluno"""
    nome = models.CharField(max_length=200, default='aluno')
    tel = models.CharField(max_length=200)
    turma = models.CharField(max_length=200)
    curso = models.CharField(max_length=200)
    matricula = models.CharField(max_length=200, primary_key=True)
