# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('projeto', '0002_ocorrencia'),
    ]

    operations = [
        migrations.AddField(
            model_name='ocorrencia',
            name='hora',
            field=models.TimeField(default=datetime.datetime(2015, 8, 5, 17, 40, 27, 438479, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
