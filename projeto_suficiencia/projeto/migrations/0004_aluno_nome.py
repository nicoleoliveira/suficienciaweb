# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projeto', '0003_ocorrencia_hora'),
    ]

    operations = [
        migrations.AddField(
            model_name='aluno',
            name='nome',
            field=models.CharField(default=None, max_length=200),
        ),
    ]
