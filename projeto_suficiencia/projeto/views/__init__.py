# coding: utf-8

from projeto.views.views import Home
from projeto.views.cadastro import CadastroProfessor, PerfilProfessor
from projeto.views.ocorrencia import CadastroOcorrencia, MostrarOcorrencias, OcorrenciaDetalhe, AlterOcorrencia
from projeto.views.aluno import CadastroAluno, MostrarAlunos, PerfilAluno
