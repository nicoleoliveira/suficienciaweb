from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic.base import View
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.shortcuts import redirect


class Home(View):
    templateProfessor = 'professor.html'
    templateAdm = 'adm.html'

    @method_decorator(login_required)
    def get(self, request):

        grupo = request.user.groups.all().first()
        if grupo is None:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

        elif grupo.name == 'professor':
            return render_to_response(
                self.templateProfessor,
                context_instance=RequestContext(request))

        elif grupo.name == 'administrador':
            return render_to_response(
                self.templateAdm,
                context_instance=RequestContext(request))

        elif grupo.name == 'aluno':
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

        else:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
